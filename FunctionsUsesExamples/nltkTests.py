#_____Importing Packages and Constants_____
#!/usr/bin/env python
from __future__ import division
import csv, sys, os, re
sys.path.append('/Users/chipdelmal/anaconda/lib/python2.7/site-packages')
import nltk
from nltk import bigrams
from nltk.collocations import *

bgm = nltk.collocations.BigramAssocMeasures()
#_____Opening Input File_____
file = open('collocationsIn.csv')
raw = file.read()
splitText = nltk.word_tokenize(raw)
#LexicalRichness
print len(splitText) / len(set(splitText))
print len(splitText)
print splitText.collocations()