#_____Importing Packages and Constants_____
import codecs
import os
import sys
sys.path.append('/Users/chipdelmal/anaconda/lib/python2.7/site-packages/lingpy2-2.0-py2.7.egg')
sys.path.append('/Users/chipdelmal/anaconda/lib/python2.7/site-packages/regex-2014.01.10-py2.7-macosx-10.5-x86_64.egg')
import codecs
from lingpy2 import *

#ReadInputFileAndSeparateStrings
with codecs.open('alignmentIn.txt', encoding='utf-8') as f:
    lines = f.read().splitlines()
#Print
#print(lines)
#Align
msa = Multiple(lines)
msa.prog_align()
#WriteOutput
ff = codecs.open("alignmentOut.txt", 'w', encoding='utf-16')
ff.write("%s" % msa)
ff.close()

