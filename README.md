# PajaroLoco #
PajaroLoco is a Mathematica package created by Instituto Tecnológico y de Estudios Superiores de Monterrey (ITESM) Campus Estado de México in conjunction with University of California, Los Angeles (UCLA) for the study of birds's linguistics and analysis of its properties. This work has been developed as an interdisciplinary project between evolutionary biologists and computer scientists.
The objective of the package is to provide a unified workbench and toolbox that allows the streamlined verification and analysis of the birds's records obtained from fieldwork.

## How do I install it? ##
* Open the folder *InstallationTutorial* for instructions on how to install the package
* Alternatively you can open the notebook: PajaroLocoPublic.m on Mathematica and run it if only a temporal "installation" is what you are looking for.
* Or if you are running on MacOS watch the videos:

** Scripted: http://youtu.be/IFtBSup-2wk

** Manual: http://youtu.be/eTozrxZThsA?list=UUYMFzbv0Kwn2-fKV8q8iifg

## Who do I talk to? ##
Héctor Manuel Sánchez Castellanos: sanchez.hmsc@itesm.mx