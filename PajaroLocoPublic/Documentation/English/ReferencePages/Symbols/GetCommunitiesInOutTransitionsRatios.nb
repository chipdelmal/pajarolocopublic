(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     12617,        505]
NotebookOptionsPosition[      8315,        349]
NotebookOutlinePosition[      8945,        374]
CellTagsIndexPosition[      8867,        369]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["PajaroLocoPublic", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["PajaroLocoPublic`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["PajaroLocoPublic/ref/GetCommunitiesInOutTransitionsRatios", \
"Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["GetCommunitiesInOutTransitionsRatios", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"GetCommunitiesInOutTransitionsRatios", "[", 
   RowBox[{"frequencyMatrixOutput", ",", "communitiesList"}], "]"}]], 
  "InlineFormula"],
 " \[LineSeparator]GetCommunitiesInOutTransitionsRatios returns a list of the \
ratios of within and outside transitions in the communities list."
}], "Usage",
 CellChangeTimes->{{3.602598163652882*^9, 3.602598192883483*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Needs", "[", "\"\<PajaroLocoPublic`\>\"", 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"randomSample", "=", 
   RowBox[{"RandomChoice", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<D\>\"", ",", 
       "\"\<E\>\""}], "}"}], ",", "10"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GetTransitionsFrequencies", "[", "randomSample", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"GetCommunitiesInOutTransitionsRatios", "[", 
  RowBox[{"%", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<A\>\"", ",", "\"\<C\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<B\>\"", ",", "\"\<D\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<E\>\"", ",", "\"\<F\>\"", ",", "\"\<G\>\""}], "}"}]}], 
    "}"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.602598044681637*^9, 3.602598119145548*^9}},
 CellLabel->"In[402]:=",
 CellID->427836402],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.`", ",", "0.5`", ",", "0.25`"}], "}"}]], "Output",
 CellChangeTimes->{{3.602598049186627*^9, 3.602598119868526*^9}},
 CellLabel->"Out[405]=",
 CellID->836531575]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[CellGroupData[{

Cell[BoxData["CountLoops"], "ExampleSubsection",
 CellChangeTimes->{{3.602598130259178*^9, 3.6025981353790817`*^9}},
 CellID->1757724783],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"randomSample", "=", 
   RowBox[{"RandomChoice", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<D\>\"", ",", 
       "\"\<E\>\""}], "}"}], ",", "10"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GetTransitionsFrequencies", "[", "randomSample", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"GetCommunitiesInOutTransitionsRatios", "[", 
  RowBox[{"%", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<A\>\"", ",", "\"\<C\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<B\>\"", ",", "\"\<D\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<E\>\"", ",", "\"\<F\>\"", ",", "\"\<G\>\""}], "}"}]}], 
    "}"}], ",", 
   RowBox[{"CountLoops", "\[Rule]", "False"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.602598148032895*^9, 3.602598153869896*^9}},
 CellLabel->"In[406]:=",
 CellID->497960251],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.`", ",", "0.3333333333333333`", ",", "0.`"}], "}"}]], "Output",
 CellChangeTimes->{3.602598154300982*^9},
 CellLabel->"Out[408]=",
 CellID->1929222388]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 86}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 20, \
2012)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[5690, 255, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 8729, 362}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 89, 2, 70, "Categorization",
 CellID->605800465],
Cell[1152, 49, 86, 2, 70, "Categorization",
 CellID->468444828],
Cell[1241, 53, 104, 2, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1382, 60, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1440, 63, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1522, 69, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1590, 72, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1687, 76, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1772, 80, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1856, 84, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1975, 91, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[2031, 94, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2097, 98, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2169, 102, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2236, 106, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2308, 110, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2372, 114, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2436, 118, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2502, 122, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2583, 126, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2687, 133, 79, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2769, 136, 448, 10, 91, "Usage",
 CellID->982511436],
Cell[3220, 148, 42, 1, 25, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3299, 154, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3359, 157, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3441, 163, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3527, 166, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3622, 172, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3690, 175, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[3776, 181, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[3834, 184, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[3914, 190, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[3974, 193, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4057, 199, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[CellGroupData[{
Cell[4438, 214, 997, 28, 88, "Input",
 CellID->427836402],
Cell[5438, 244, 203, 5, 23, "Output",
 CellID->836531575]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5690, 255, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[5793, 259, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[5921, 264, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[6094, 271, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[CellGroupData[{
Cell[6246, 278, 137, 2, 20, "ExampleSubsection",
 CellID->1757724783],
Cell[CellGroupData[{
Cell[6408, 284, 964, 27, 73, "Input",
 CellID->497960251],
Cell[7375, 313, 193, 5, 23, "Output",
 CellID->1929222388]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[7607, 323, 131, 3, 33, "ExampleSection",
 CellID->258228157],
Cell[7741, 328, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[7886, 333, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[8024, 338, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[8167, 343, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
