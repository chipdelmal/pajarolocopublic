(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     10365,        428]
NotebookOptionsPosition[      6353,        284]
NotebookOutlinePosition[      6983,        309]
CellTagsIndexPosition[      6905,        304]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["PajaroLocoPublic", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["PajaroLocoPublic`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["PajaroLocoPublic/ref/GetRepertoireSize", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["GetRepertoireSize", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"GetRepertoireSize", "[", "song", "]"}]], "InlineFormula"],
 " \[LineSeparator]GetRepertoireSize returns the number of different phrases \
used in a sample."
}], "Usage",
 CellChangeTimes->{{3.602593324225606*^9, 3.6025933278093147`*^9}, {
  3.6025934423855762`*^9, 3.6025934606065273`*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Needs", "[", "\"\<PajaroLocoPublic`\>\"", 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"randomSample", "=", 
   RowBox[{"RandomChoice", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<D\>\"", ",", 
       "\"\<E\>\""}], "}"}], ",", "30"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"GetRepertoireSize", "[", "randomSample", "]"}]}], "Input",
 CellChangeTimes->{{3.602593464874589*^9, 3.602593472458825*^9}, {
  3.602593507855105*^9, 3.60259352031271*^9}},
 CellLabel->"In[77]:=",
 CellID->825874061],

Cell[BoxData["5"], "Output",
 CellChangeTimes->{{3.6025935163289623`*^9, 3.602593520643866*^9}},
 CellLabel->"Out[79]=",
 CellID->1651351854]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 43}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 20, \
2012)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[5133, 239, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 6767, 297}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 89, 2, 70, "Categorization",
 CellID->605800465],
Cell[1152, 49, 86, 2, 70, "Categorization",
 CellID->468444828],
Cell[1241, 53, 83, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1361, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1419, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1501, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1569, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1666, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1751, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1835, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1954, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[2010, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2076, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2148, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2215, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2287, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2351, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2415, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2481, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2562, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2666, 132, 60, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2729, 135, 377, 9, 75, "Usage",
 CellID->982511436],
Cell[3109, 146, 42, 1, 25, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3188, 152, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3248, 155, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3330, 161, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3416, 164, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3511, 170, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3579, 173, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[3665, 179, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[3723, 182, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[3803, 188, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[3863, 191, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[3946, 197, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[CellGroupData[{
Cell[4327, 212, 613, 16, 57, "Input",
 CellID->825874061],
Cell[4943, 230, 141, 3, 23, "Output",
 CellID->1651351854]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5133, 239, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[5236, 243, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[5364, 248, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[5515, 253, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[5645, 258, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[5779, 263, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[5924, 268, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[6062, 273, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[6205, 278, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
