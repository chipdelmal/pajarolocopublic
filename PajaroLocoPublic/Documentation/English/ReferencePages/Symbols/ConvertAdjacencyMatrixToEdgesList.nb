(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     19067,        669]
NotebookOptionsPosition[     14592,        507]
NotebookOutlinePosition[     15224,        532]
CellTagsIndexPosition[     15145,        527]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["PajaroLocoPublic", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["PajaroLocoPublic`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["PajaroLocoPublic/ref/ConvertAdjacencyMatrixToEdgesList", \
"Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["ConvertAdjacencyMatrixToEdgesList", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"ConvertAdjacencyMatrixToEdgesList", "[", "adjacencyMatrix", "]"}]],
   "InlineFormula"],
 " \[LineSeparator]ConvertAdjacencyMatrixToEdgesList transforms and adjacency \
matrix into an edges list."
}], "Usage",
 CellChangeTimes->{{3.60259685033958*^9, 3.60259687561696*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Needs", "[", "\"\<PajaroLocoPublic`\>\"", 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"randomSample", "=", 
   RowBox[{"RandomChoice", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<D\>\"", ",", 
       "\"\<E\>\""}], "}"}], ",", "30"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GetTransitionsMarkov", "[", "randomSample", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"ConvertAdjacencyMatrixToEdgesList", "[", "%", "]"}]}], "Input",
 CellChangeTimes->{{3.602596888367921*^9, 3.6025969237913237`*^9}, {
  3.602884443106434*^9, 3.6028844553418016`*^9}},
 CellLabel->"In[8]:=",
 CellID->948262522],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"B\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"A\"\>", ",", "0.25`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"E\"\>", ",", "0.5`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"C\"\>", ",", "0.25`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"D\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"B\"\>", ",", "0.125`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"A\"\>", ",", "0.5`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"E\"\>", ",", "0.125`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"C\"\>", ",", "0.125`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"D\"\>", ",", "0.125`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"B\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"A\"\>", ",", "0.2`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"E\"\>", ",", "0.2`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"C\"\>", ",", "0.4`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"D\"\>", ",", "0.2`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"B\"\>", ",", "0.2857142857142857`"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"A\"\>", ",", "0.2857142857142857`"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"E\"\>", ",", "0.14285714285714285`"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"C\"\>", ",", "0.14285714285714285`"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"D\"\>", ",", "0.14285714285714285`"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"B\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"A\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"E\"\>", ",", "0.2`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"C\"\>", ",", "0.4`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"D\"\>", ",", "0.4`"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.602596918724992*^9, 3.602596924074161*^9}, 
   3.602884458568021*^9},
 CellLabel->"Out[11]=",
 CellID->27694188]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"randomSample", "=", 
   RowBox[{"RandomChoice", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<D\>\"", ",", 
       "\"\<E\>\""}], "}"}], ",", "30"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GetTransitionsFrequencies", "[", "randomSample", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"ConvertAdjacencyMatrixToEdgesList", "[", "%", "]"}]}], "Input",
 CellChangeTimes->{{3.602596931393641*^9, 3.602596932391164*^9}},
 CellLabel->"In[320]:=",
 CellID->1557943228],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"D\"\>", ",", "3.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"B\"\>", ",", "3.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"C\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"A\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"E\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"D\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"B\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"C\"\>", ",", "3.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"A\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"E\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"D\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"B\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"C\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"A\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"E\"\>", ",", "3.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"D\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"B\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"C\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"A\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"E\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"D\"\>", ",", "3.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"B\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"C\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"A\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"E\"\>", ",", "3.`"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.602596932973618*^9},
 CellLabel->"Out[322]=",
 CellID->1620298126]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[CellGroupData[{

Cell[BoxData["AllowLoops"], "ExampleSubsection",
 CellChangeTimes->{{3.602596956577442*^9, 3.602596958482937*^9}},
 CellID->1757724783],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"randomSample", "=", 
   RowBox[{"RandomChoice", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\"\<A\>\"", ",", "\"\<B\>\"", ",", "\"\<C\>\"", ",", "\"\<D\>\"", ",", 
       "\"\<E\>\""}], "}"}], ",", "30"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GetTransitionsFrequencies", "[", "randomSample", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"ConvertAdjacencyMatrixToEdgesList", "[", 
  RowBox[{"%", ",", 
   RowBox[{"AllowLoops", "\[Rule]", "False"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.602596968834858*^9, 3.602596973719286*^9}},
 CellLabel->"In[323]:=",
 CellID->1345521517],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"D\"\>", ",", "2.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"A\"\>", ",", "2.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"C\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"E\"\>", ",", "\<\"B\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"E\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"A\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"C\"\>", ",", "2.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"D\"\>", ",", "\<\"B\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"E\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"D\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"C\"\>", ",", "3.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"A\"\>", ",", "\<\"B\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"E\"\>", ",", "4.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"D\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"A\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"C\"\>", ",", "\<\"B\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"E\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"D\"\>", ",", "0.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"A\"\>", ",", "1.`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"B\"\>", ",", "\<\"C\"\>", ",", "1.`"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.602596974108684*^9},
 CellLabel->"Out[325]=",
 CellID->1179522112]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 38}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 20, \
2012)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[10579, 381, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 15006, 520}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 89, 2, 70, "Categorization",
 CellID->605800465],
Cell[1152, 49, 86, 2, 70, "Categorization",
 CellID->468444828],
Cell[1241, 53, 101, 2, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1379, 60, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1437, 63, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1519, 69, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1587, 72, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1684, 76, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1769, 80, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1853, 84, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1972, 91, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[2028, 94, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2094, 98, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2166, 102, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2233, 106, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2305, 110, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2369, 114, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2433, 118, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2499, 122, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2580, 126, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2684, 133, 76, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2763, 136, 360, 9, 75, "Usage",
 CellID->982511436],
Cell[3126, 147, 42, 1, 25, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3205, 153, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3265, 156, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3347, 162, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3433, 165, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3528, 171, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3596, 174, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[3682, 180, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[3740, 183, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[3820, 189, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[3880, 192, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[3963, 198, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[CellGroupData[{
Cell[4344, 213, 728, 19, 73, "Input",
 CellID->948262522],
Cell[5075, 234, 2499, 62, 142, "Output",
 CellID->27694188]
}, Open  ]],
Cell[CellGroupData[{
Cell[7611, 301, 597, 16, 57, "Input",
 CellID->1557943228],
Cell[8211, 319, 2319, 56, 84, "Output",
 CellID->1620298126]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10579, 381, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[10682, 385, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[10810, 390, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[10983, 397, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[CellGroupData[{
Cell[11135, 404, 135, 2, 20, "ExampleSubsection",
 CellID->1757724783],
Cell[CellGroupData[{
Cell[11295, 410, 663, 18, 57, "Input",
 CellID->1345521517],
Cell[11961, 430, 1884, 46, 69, "Output",
 CellID->1179522112]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[13884, 481, 131, 3, 33, "ExampleSection",
 CellID->258228157],
Cell[14018, 486, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[14163, 491, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[14301, 496, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[14444, 501, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
