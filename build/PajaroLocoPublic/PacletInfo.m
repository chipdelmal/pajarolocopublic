(* Paclet Info File *)

(* created 2014/02/28*)

Paclet[
    Name -> "PajaroLocoPublic",
    Version -> "0.0.1",
    MathematicaVersion -> "9+",
    Description -> "Animal language analysis package created by Bioinformatics group in Instituto Tecnologico de Monterrey, Campus Estado de Mexico in Collaboration with UCLA's Bird's Language research group.",
    Creator -> "Hector Manuel Sanchez Castellanos",
    Extensions -> 
        {
            {"Documentation", Language -> "English"}
        }
]


