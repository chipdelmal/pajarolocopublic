#_____Importing Packages and Constants_____
#!/usr/bin/env python
import csv, sys, os, re
sys.path.append('/Users/chipdelmal/anaconda/lib/python2.7/site-packages')#CHANGE THIS LINE TO YOUR PYTHON INSTALLATION FOLDER (USUALLY ONLY THE USERNAME chipdelmal NEEDS TO BE CHANGED IN UNIX SYSTEMS)
import nltk
from nltk import bigrams
from nltk.collocations import *

bgm = nltk.collocations.BigramAssocMeasures()
#_____Opening Input File_____
file = open('collocationsIn.csv')
raw = file.read()
splitText = nltk.word_tokenize(raw)
#_____Analyzing Collocations_____
finder = BigramCollocationFinder.from_words(splitText,window_size=2)
score = finder.score_ngrams(bgm.likelihood_ratio)#(bgm.pmi)
frequency = finder.ngram_fd.viewitems()
#_____Printing and Exporting Data_____
csv_writer = csv.writer(sys.stdout, delimiter=',')
csvFileScore = csv_writer.writerows(score)
csvFileScore = csv_writer.writerows(frequency)
outputFilenameScore = ('outputScored%s' % 'collocationsOut') + '.csv'
outputFilenameFrequency = ('outputFrequency%s' % 'collocationsOut') + '.csv'
with open(outputFilenameScore, 'w') as f:
    w = csv.writer(f, dialect = 'excel')
    w.writerows(score)
with open(outputFilenameFrequency, 'w') as f:
    w = csv.writer(f, dialect = 'excel')
    w.writerows(frequency)