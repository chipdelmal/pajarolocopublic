(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      4198,        105]
NotebookOptionsPosition[      3872,         88]
NotebookOutlinePosition[      4227,        104]
CellTagsIndexPosition[      4184,        101]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{"<<", "PajaroLocoPublic`"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"allFilesPaths", "=", 
   RowBox[{"FileNames", "[", 
    RowBox[{
     RowBox[{"header", "<>", "\"\<*\>\"", "<>", "\"\<.csv\>\""}], ",", 
     RowBox[{"{", "\"\<SampleFiles\>\"", "}"}], ",", "Infinity"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"samples", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"Import", "[", "#", "]"}], "&"}], "/@", 
   "allFilesPaths"}]}]}], "Input",
 CellChangeTimes->{{3.6046128038406754`*^9, 3.6046128467474194`*^9}, {
  3.6046591371975393`*^9, 3.604659192788755*^9}, {3.604659247985413*^9, 
  3.604659318755866*^9}, {3.604659435882902*^9, 3.604659451532464*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\<\"a\"\>", ",", "\<\"f\"\>", ",", "\<\"a\"\>", 
       ",", "\<\"s\"\>", ",", "\<\"a\"\>", ",", "\<\"r\"\>", ",", "\<\"t\"\>",
        ",", "\<\"q\"\>", ",", "\<\"a\"\>", ",", "\<\"s\"\>", 
       ",", "\<\"c\"\>", ",", "\<\"z\"\>", ",", "\<\"g\"\>", ",", "\<\"g\"\>",
        ",", "\<\"h\"\>", ",", "\<\"q\"\>", ",", "\<\"y\"\>", 
       ",", "\<\"i\"\>", ",", "\<\"a\"\>", ",", "\<\"z\"\>", ",", "\<\"g\"\>",
        ",", "\<\"s\"\>", ",", "\<\"t\"\>", ",", "\<\"y\"\>", 
       ",", "\<\"a\"\>", ",", "\<\"w\"\>", ",", "\<\"a\"\>", ",", "\<\"h\"\>",
        ",", "\<\"h\"\>", ",", "\<\"h\"\>", ",", "\<\"y\"\>", 
       ",", "\<\"a\"\>"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\<\"a\"\>", ",", "\<\"s\"\>", ",", "\<\"c\"\>", 
       ",", "\<\"z\"\>", ",", "\<\"g\"\>", ",", "\<\"g\"\>", ",", "\<\"h\"\>",
        ",", "\<\"q\"\>", ",", "\<\"y\"\>", ",", "\<\"i\"\>", 
       ",", "\<\"h\"\>", ",", "\<\"y\"\>", ",", "\<\"a\"\>", ",", "\<\"a\"\>",
        ",", "\<\"f\"\>", ",", "\<\"a\"\>", ",", "\<\"s\"\>", 
       ",", "\<\"a\"\>", ",", "\<\"r\"\>", ",", "\<\"t\"\>", ",", "\<\"q\"\>",
        ",", "\<\"a\"\>", ",", "\<\"z\"\>", ",", "\<\"g\"\>", 
       ",", "\<\"s\"\>", ",", "\<\"t\"\>", ",", "\<\"y\"\>", ",", "\<\"a\"\>",
        ",", "\<\"w\"\>", ",", "\<\"a\"\>", ",", "\<\"h\"\>", 
       ",", "\<\"h\"\>", ",", "\<\"\"\>"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"{", 
     RowBox[{"\<\"a\"\>", ",", "\<\"f\"\>", ",", "\<\"a\"\>", 
      ",", "\<\"s\"\>", ",", "\<\"a\"\>", ",", "\<\"r\"\>", ",", "\<\"t\"\>", 
      ",", "\<\"q\"\>", ",", "\<\"i\"\>", ",", "\<\"a\"\>", ",", "\<\"z\"\>", 
      ",", "\<\"g\"\>", ",", "\<\"s\"\>", ",", "\<\"h\"\>", ",", "\<\"h\"\>", 
      ",", "\<\"y\"\>", ",", "\<\"a\"\>", ",", "\<\"a\"\>", ",", "\<\"s\"\>", 
      ",", "\<\"c\"\>", ",", "\<\"z\"\>", ",", "\<\"g\"\>", ",", "\<\"g\"\>", 
      ",", "\<\"h\"\>", ",", "\<\"q\"\>", ",", "\<\"y\"\>", ",", "\<\"t\"\>", 
      ",", "\<\"y\"\>", ",", "\<\"a\"\>", ",", "\<\"w\"\>", ",", "\<\"a\"\>", 
      ",", "\<\"h\"\>", ",", "\<\"\"\>"}], "}"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.6046591556249037`*^9, 3.6046592116854887`*^9}, {
   3.604659249048901*^9, 3.6046593196734333`*^9}, {3.6046594440637608`*^9, 
   3.604659452002149*^9}, 3.60466961637071*^9, {3.604669653348638*^9, 
   3.604669662618246*^9}}]
}, Open  ]]
},
WindowSize->{740, 727},
WindowMargins->{{10, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 824, 20, 80, "Input"],
Cell[1406, 44, 2450, 41, 155, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
